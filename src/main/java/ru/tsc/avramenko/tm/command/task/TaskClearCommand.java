package ru.tsc.avramenko.tm.command.task;

import ru.tsc.avramenko.tm.command.AbstractTaskCommand;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Removing all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        serviceLocator.getTaskService().clear();
        System.out.println("[OK]");
    }

}