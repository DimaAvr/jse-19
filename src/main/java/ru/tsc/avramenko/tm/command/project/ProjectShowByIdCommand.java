package ru.tsc.avramenko.tm.command.project;

import ru.tsc.avramenko.tm.command.AbstractProjectCommand;
import ru.tsc.avramenko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.avramenko.tm.model.Project;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show a project by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findById(id);
        if (project == null) throw new ProjectNotFoundException();
        showProject(project);
    }

}