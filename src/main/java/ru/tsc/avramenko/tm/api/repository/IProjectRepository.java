package ru.tsc.avramenko.tm.api.repository;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    boolean existsById(String id);

    Project findByName(String name);

    Project findByIndex(int index);

    Project removeByName(String name);

    Project removeByIndex(int index);

    Project startById(String id);

    Project startByName(String name);

    Project startByIndex(int index);

    Project finishById(String id);

    Project finishByName(String name);

    Project finishByIndex(int index);

    Project changeStatusById(String id, Status status);

    Project changeStatusByName(String name, Status status);

    Project changeStatusByIndex(int index, Status status);

}
