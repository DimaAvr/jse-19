package ru.tsc.avramenko.tm.api.service;

import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    void create(String name);

    void create(String name, String description);

    Project changeStatusById(String id, Status status);

    Project changeStatusByName(String name, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    List<Project> findAll(Comparator<Project> comparator);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project startById(String id);

    Project startByName(String name);

    Project startByIndex(Integer index);

    Project finishById(String id);

    Project finishByName(String name);

    Project finishByIndex(Integer index);

}