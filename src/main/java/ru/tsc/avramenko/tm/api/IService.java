package ru.tsc.avramenko.tm.api;

import ru.tsc.avramenko.tm.model.AbstractEntity;

import java.util.List;

public interface IService <E extends AbstractEntity> extends IRepository<E> {

    List<E> findAll();

    void add(final E entity);

    void remove(final E entity);

    void clear();

    E findById(final String id);

    E removeById(final String id);

}