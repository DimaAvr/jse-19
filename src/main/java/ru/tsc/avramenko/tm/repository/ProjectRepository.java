package ru.tsc.avramenko.tm.repository;

import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(list);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public boolean existsById(final String id) {
        return findById(id) != null;
    }

    @Override
    public Project findByName(final String name) {
        for (Project project : list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Project removeByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project startById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByName(final String name) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByIndex(final int index) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        final Project project = findById(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        final Project project = findByName(name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(int index, Status status) {
        final Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}