package ru.tsc.avramenko.tm.exception.empty;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class EmptyFullNameException extends AbstractException {

    public EmptyFullNameException() {
        super("Error! Full name cannot be empty.");
    }

}
